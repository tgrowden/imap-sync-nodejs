#imap-sync-nodejs
================

This is a fork of [nihaopaul's imap-sync-nodejs](https://github.com/nihaopaul/imap-sync-nodejs) with an updated UI and more options for sync settings. It uses the invaluable tool, [imapsync](https://github.com/imapsync/imapsync).

NB: This is assuming a Linux install with nodejs/npm already installed--OSX/Windows users, you're on your own.

1. `$ git clone https://github.com/tgrowden/imap-sync-nodejs.git`
2. `$ cd imap-sync-nodejs`
3. `$ npm install`
4. Install modules for imapsync: `$ ./imapsync/examples/install_modules_linux.sh` (may have to run as root)
5. `$ npm start` or `$ pm2 start bin/www`
6. Check it out: http://localhost:3000


Tested on Arch Linux and CentOS--YMMV
